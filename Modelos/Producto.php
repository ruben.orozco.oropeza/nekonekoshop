<?php
namespace Modelos;
//require_once('../config/cargador.php'); //Comentar para usar en las vistas
use Modelos\Conexion;

class Producto {

  public $productoId;
  public $nombre;
  public $descripcion;
  public $imgProducto;
  public $precio;

  public function __construct($producto = []) {
    $this->productoId = $producto["producto_id"] ?? 0;
    $this->nombre = $producto["nombre"] ?? "";
    $this->descripcion = $producto["descripcion"] ?? "";
    $this->precio = $producto["precio"] ?? 0.0;
    $this->imgProducto = $producto["img_producto"] ?? "";
  }

  public static function consultar($productoId) {
    $sql = "SELECT * FROM productos WHERE producto_id = :producto_id";
    $parametros = [
      'producto_id' => $productoId
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    $productoDatos = $resultados->fetch();
    return new Producto($productoDatos);
  }

  public static function existe($productoId) {
    $sql = "SELECT * FROM productos WHERE producto_id = :productoId";
    $parametros = [
      'productoId' => $productoId
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    $numProductos = $resultados->rowCount();
    return 0 < $numProductos;
  }

  public static function buscar($busqueda = '') {
    $sql = "
      SELECT P.producto_id, P.nombre, P.descripcion, P.precio, P.img_producto
      FROM productos P
      WHERE 1 = 1
    ";
    $parametros = [];

    if (!empty($busqueda)) {
      $sql .= " AND (lower(nombre) LIKE CONCAT('%', lower(:busqueda), '%') OR lower(descripcion) LIKE CONCAT('%', lower(:busqueda), '%'))";
      $parametros['busqueda'] = $busqueda;
    }
    $sql .= " ORDER BY P.producto_id DESC";
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  public static function listar() {
    $sql = "SELECT * FROM productos";
    $parametros = [];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  public static function total($ordenId) {
    $sql = "
      SELECT SUM(precio_final * cantidad) as total
      FROM  orden_producto
      WHERE orden_id = :ordenId
    ";
    $parametros = [
      'ordenId' => $ordenId
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  private function insertar(){
    $sql = "
      INSERT INTO productos (nombre, descripcion, precio, img_producto)
      VALUES (:nombre, :descripcion, :precio, :imgProducto);
    ";
    $parametros = [
      ':nombre' => $this->nombre,
      ':descripcion' => $this->descripcion,
      ':precio' => $this->precio,
      ':imgProducto' => $this->imgProducto,
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  private function actualizar() {
    $sql = "
      UPDATE productos
      SET
        descripcion = :descripcion,
        nombre = :nombre,
        precio = :precio,
        img_producto = :imgProducto
      WHERE
        producto_id = :productoId
    ";
    $parametros = [
      ':nombre' => $this->nombre,
      ':descripcion' => $this->descripcion,
      ':precio' => $this->precio,
      ':imgProducto' => $this->imgProducto,
      ':productoId' => $this->productoId,
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  public function guardar() {
    if (self::existe($this->productoId)) {
      return $this->actualizar();
    } else {
      return $this->insertar(); 
    }
  }

  public function borrar() {
    $sql = "DELETE FROM productos WHERE producto_id = :productoId";
    $parametros = [
      ':productoId' => $this->productoId
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  static public function actualizarPrecio($productoId, $precio) {
    $sql = "UPDATE productos SET precio = :precio WHERE producto_id = :productoId";
    $parametros = [
      ':precio' => $precio,
      ':productoId' => $productoId
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }
}
