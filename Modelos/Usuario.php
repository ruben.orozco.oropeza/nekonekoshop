<?php
namespace Modelos;
//require_once('../config/cargador.php'); //Comentar para usar en las vistas
use Modelos\Conexion;

class Usuario {

  public $usuarioId;
  public $nombreUsuario;
  public $email;
  public $password;
  public $nombre;
  public $admin;

  public function __construct($usuario = []) {
    $this->usuarioId = $usuario['usuario_id'] ?? 0;
    $this->nombreUsuario = htmlentities($usuario['nombre_usuario'] ?? '');
    $this->email = htmlentities($usuario['email'] ?? '');
    $this->password = htmlentities($usuario['password'] ?? '');
    $this->nombre = htmlentities($usuario['nombre'] ?? '');
    $this->admin = htmlentities($usuario['admin'] ?? false);
  }

  public static function consultar($usuarioId = 0, $nombreUsuario = '') {
    $sql = "SELECT * FROM usuarios WHERE 1 = 1";
    $parametros = [];
    if (!empty($usuarioId)) {
      $sql .= " AND usuario_id = :usuario_id";
      $parametros[':usuario_id'] = $usuarioId;
    }

    if (!empty($nombreUsuario)) {
      $sql .= " AND nombre_usuario = :nombre_usuario";
      $parametros[':nombre_usuario'] = $nombreUsuario;
    }

    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    $usuarioDatos = $resultados->fetch();
    return new Usuario($usuarioDatos);
  }

  public static function existe($usuarioId) {
    $sql = "SELECT * FROM usuarios U WHERE U.usuario_id = :usuarioId";
    $parametros = [
      'usuarioId' => $usuarioId
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    $numUsuarios = $resultados->rowCount();
    return 0 < $numUsuarios;
  }

  public static function existeNombre($usuarioNombre) {
    $sql = "SELECT * FROM usuarios U WHERE U.nombre_usuario = :usuarioNombre";
    $parametros = [
      'usuarioNombre' => $usuarioNombre
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    $numUsuarios = $resultados->rowCount();
    return 0 < $numUsuarios;
  }

  public static function listar() {
    $sql = "
      SELECT U.usuario_id, U.nombre_usuario, U.email, U.nombre, U.password, U.admin
      FROM usuarios U
      ORDER BY U.usuario_id
    ";
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql);

    return $resultados;
  }

  private function insertar(){
    $sql = "
      INSERT INTO usuarios (nombre_usuario, password, email, nombre, admin)
      VALUES (:nombreUsuario, :password, :email, :nombre, :admin)
    ";
    $parametros = [
      ':nombreUsuario' => $this->nombreUsuario,
      ':email' => $this->email,
      ':password' => $this->password,
      ':nombre' => $this->nombre,
      ':admin' => $this->admin
    ];
    $conexion = new Conexion();

    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  private function actualizar() {
    $sql = "
      UPDATE usuarios
      SET
        nombre_usuario = :nombreUsuario,
        email = :email,
        password = :password,
        nombre = :nombre,
        admin = :admin
      WHERE
        usuario_id = :usuarioId
    ";
    $parametros = [
      ':usuarioId' => $this->usuarioId,
      ':nombreUsuario' => $this->nombreUsuario,
      ':email' => $this->email,
      ':password' => $this->password,
      ':nombre' => $this->nombre,
      ':admin' => $this->admin
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  private function setAdmin() {
    $sql = "UPDATE usuarios SET admin = :admin WHERE usuario_id = :usuarioId";
    $parametros = [
      ':admin' => $this->admin,
      ':usuarioId' => $this->usuarioId
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  public function guardar($promover = false) {
    if (self::existe($this->usuarioId)) {
      if($promover == true) {
        return $this->setAdmin();
      } else {
        return $this->actualizar();
      }
    } else {
      return $this->insertar();
    }
  }

  public function borrar() {
    $sql = "DELETE FROM usuarios WHERE usuario_id = :usuarioId";
    $parametros = [
      ':usuarioId' => $this->usuarioId
    ];
    $conexion = new Conexion();
    $resultados = $conexion->correrQuery($sql, $parametros);
    return $resultados;
  }

  public static function validarLogin($nombreUsuario, $password) {
    $res = '';
    $usuario = self::consultar(0, $nombreUsuario);

    if (empty($usuario->usuarioId)) {
      $res = 'Usuario invalido';
    } else {
      if ($usuario->password !== $password) {
        $res = 'Password Invalido';
      } else {
        $res = $usuario;
      }
    }
    return $res;
  }
}
?>