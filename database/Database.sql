USE rubendb;

CREATE TABLE usuarios (
  usuario_id INT AUTO_INCREMENT PRIMARY KEY,
  nombre_usuario VARCHAR(64) NOT NULL,
  email VARCHAR(64),
  password VARCHAR(256) NOT NULL,
  nombre VARCHAR(256) NOT NULL,
  admin BOOLEAN NOT NULL
);

CREATE TABLE direcciones (
  direccion_id INT AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(64) NOT NULL,
  calle_numero VARCHAR(128) NOT NULL,
  cp VARCHAR(32) NOT NULL,
  colonia VARCHAR(64) NOT NULL,
  municipio VARCHAR(64) NOT NULL,
  estado VARCHAR(64) NOT NULL,
  usuario_id INT NOT NULL,
  UNIQUE(usuario_id, nombre),
  FOREIGN KEY(usuario_id) REFERENCES usuarios(usuario_id)
);

CREATE TABLE tipo_producto (
  tipo_producto_id INT AUTO_INCREMENT PRIMARY KEY,
  tipo_producto_nombre VARCHAR(64) NOT NULL
);

CREATE TABLE productos (
  producto_id INT AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(64) NOT NULL,
  descripcion VARCHAR(2048),
  img_producto VARCHAR(128),
  precio FLOAT(8,2)
);

CREATE TABLE producto_clasificacion (
  producto_clasificacion_id INT AUTO_INCREMENT PRIMARY KEY,
  tipo_producto_id INT NOT NULL,
  producto_id INT NOT NULL,
  FOREIGN KEY(tipo_producto_id) REFERENCES tipo_producto(tipo_producto_id),
  FOREIGN KEY(producto_id) REFERENCES productos(producto_id),
  UNIQUE(tipo_producto_id, producto_id)
);

CREATE TABLE ordenes (
  orden_id INT AUTO_INCREMENT PRIMARY KEY,
  comprador_id INT NOT NULL,
  status VARCHAR(64),
  direccion_id int,
  fecha_compra DATE DEFAULT (NOW()),
  fecha_entrega DATE DEFAULT (NOW() + INTERVAL 10 DAY),
  FOREIGN KEY(comprador_id) REFERENCES usuarios(usuario_id),
  FOREIGN KEY(direccion_id) REFERENCES direcciones(direccion_id)
);

CREATE TABLE orden_producto(
  orden_producto_id INT AUTO_INCREMENT PRIMARY KEY,
  orden_id INT NOT NULL,
  producto_id INT NOT NULL,
  precio_final FLOAT,
  cantidad INT DEFAULT 1,
  FOREIGN KEY(orden_id) REFERENCES ordenes(orden_id)
);

-- Catalogos
INSERT INTO tipo_producto(tipo_producto_nombre) VALUES ('Figura');
INSERT INTO tipo_producto(tipo_producto_nombre) VALUES ('Nendoroid');
INSERT INTO tipo_producto(tipo_producto_nombre) VALUES ('Peluches');
INSERT INTO tipo_producto(tipo_producto_nombre) VALUES ('Figuras Intercambiables');
INSERT INTO tipo_producto(tipo_producto_nombre) VALUES ('Model Kits');

INSERT INTO usuarios(nombre_usuario, password, email, nombre, admin) VALUES
  ('admin', 'admin', 'admin@test.com', 'Admin', true)
;
INSERT INTO usuarios(nombre_usuario, password, email, nombre, admin) VALUES
  ('test', 'test', 'test@test.com', 'Cliente', false)
;

INSERT INTO direcciones(nombre, calle_numero, cp, colonia, municipio, estado, usuario_id) VALUES
  ('Direccion1', 'Calle', '1', 'Colonia', 'Tlalpan', 'CDMX', 1)
;
INSERT INTO direcciones(nombre, calle_numero, cp, colonia, municipio, estado, usuario_id) VALUES
  ('Direccion2', 'Calle', '2', 'Colonia', 'Coyoacán', 'CDMX', 2)
;

INSERT INTO productos(nombre, descripcion, img_producto, precio) VALUES
  ('Nendoroid Doll Wolf: Ash', 'La serie Nendoroid Doll de figuras presenta Nendoroids con cuerpos altamente articulados y que pueden ser fácilmente vestidos con diferentes trajes. El siguiente personaje original en unirse a la serie Nendoroid Doll es “Wolf: Ash”, un chico amable y de espíritu libre. Sus orejas y pelaje de lobo son removibles. Las patas de lobo pueden ser cambiadas por partes de manos normales. La parte de la cabeza del Nendoroid puede ser fácilmente intercambiada con otros Nendoroids, ¡permitiéndote vestir a tus personajes favoritos con un nuevo atuendo!', 'productos/gsc17814_1.jpg', 1200.00),
  ('Nendoroid Doll Little Red Riding Hood: Rose', 'La serie Nendoroid Doll de figuras presenta Nendoroids con cuerpos altamente articulados y que pueden ser fácilmente vestidos con diferentes trajes. El siguiente personaje original en unirse a la serie Nendoroid es “Little Red Riding Hood: Rose”, una chica calmada y de corazón gentil. Su capucha y delantal son removibles. La parte de la cabeza del Nendoroid puede ser fácilmente intercambiada con otros Nendoroids, ¡permitiéndote vestir a tus personajes favoritos con un nuevo atuendo!', 'productos/gsc12262_1.jpg', 1200.00),
  ('Nendoroid Doll Vampire: Camus', 'La serie Nendoroid Doll de figuras presenta Nendoroids con cuerpos altamente articulados y que pueden ser fácilmente vestidos con diferentes trajes. El siguiente personaje original en unirse a la serie Nendoroid Doll es “Vampire: Camus”, un noble y sabio hermano mayor. La capa puede quitarse si se desea mostrar su intrincado traje debajo de ella. La parte de la cabeza del Nendoroid puede ser fácilmente intercambiada con otros Nendoroids, permitiéndote vestir a tus personajes favoritos con un nuevo traje para convertirlos en vampiros. ¡Haz tu pedido hoy!', 'productos/gsc12688_1.jpg', 1200.00),
  ('Nendoroid Doll Vampire: Milla', 'La serie Nendoroid Doll de figuras presenta Nendoroids con cuerpos altamente articulados y que pueden ser fácilmente vestidos con diferentes trajes. El siguiente personaje original en unirse a la serie Nendoroid Doll es “Vampire: Milla”, una astuta y traviesa hermana menor. Puedes quitarle la capa para ver el intrincado traje que lleva debajo. La parte de la cabeza del Nendoroid puede ser fácilmente intercambiada con otros Nendoroids, permitiéndote vestir a tus personajes favoritos con un nuevo atuendo de vampiro. ¡Consiguela hoy!', 'productos/gsc12689_1.jpg', 1200.00),
  ('Nendoroid Watson Amelia', '¡Watson Amelia del popular grupo Hololive Production se une a la familia de figuras Nendoroid! Viene con tres expresiones faciales intercambiables: sonrisa, mirada alegre con los ojos cerrados, y una expresión presumida. También viene con su importante lupa, un bigote falso, y su amigo canino Bubba. Pídela hoy mismo para tu colección.', 'productos/gsc17569_1.jpg', 900.00),
  ('Nendoroid Mori Calliope', '¡Mori Calliope del popular grupo grupo Hololive Production es ahora una adorable figura Nendoroid! Viene con tres caras intercambiables: ¡una sonrisa, una cara rara y una expresión rapeando! Incluye tambien un micrófono, un par de gafas de sol y una copa de vino. Pídela hoy mismo para tu colección.', 'productos/gsc17450_1.jpg', 900.00),
  ('Nendoroid Tsunomaki Watame', '¡La adorable oveja Tsunomaki Watame del grupo Hololive Production es ahora una figura Nendoroid! Sus expresiones faciales intercambiables incluyen una sonrisa, una sonrisa con los ojos cerrados, y una cara ansiosa. Ella viene con su arpa, su micrófono, y una señal de piedra-papel-tijeras. Pídela hoy mismo para tu colección.', 'productos/gsc17316_1.jpg', 900.00),
  ('Nendoroid Gawr Gura', 'Del popular grupo Hololive Production ¡llega una Nendoroid de la VTuber Gawr Gura! Ella viene con tres caras intercambiables, incluyendo una expresion estandar y dos expresiones sonrientes: una mostrando sus afilados dientes y otra con los ojos cerrados. También viene con su amigo Bloop y su tridente como piezas opcionales. La capucha de Gura también se puede quitar, ¡así que disfruta cambiando su aspecto para crear todo tipo de poses!', 'productos/gsc12594_1.jpg', 900.00),
  ('Nendoroid Houshou Marine', '¡Del grupo Hololive Production viene una Nendoroid de Houshou Marine! Viene con tres diferentes expresiones, incluyendo una expresión neutral, una expresión emocionada y una expresión guiñando un ojo. También incluye partes especiales de brazo para mostrarla llevando su abrigo pirata. Otras partes incluyen su taza de calavera, un sombrero pirata, y un parche en el ojo que ella puede llevar, ¡permitiéndote crear todo tipo de poses y situaciones en forma Nendoroid!', 'productos/gsc12587_1.jpg', 900.00),
  ('POP UP PARADE Inugami Korone', 'Del grupo Hololive Production llega una nueva figura POP UP PARADE, ¡esta vez de Inugami Korone!', 'productos/gsc94398_1.jpg', 500.00),
  ('POP UP PARADE Nekomata Okayu', '¡Del grupo Hololive Production llega una nueva figura POP UP PARADE de Nekomata Okayu! Ella ha sido esculpida con gran detalle, desde sus orejas adorables hasta su ropa de calle de la cadera. ¡Añádela a tu colección haciendo tu pedido hoy mismo!', 'productos/gsc94399_1.jpg', 500.00)
;

INSERT INTO producto_clasificacion(tipo_producto_id, producto_id) VALUES
  (2,1),
  (2,2),
  (2,3),
  (2,4),
  (2,5),
  (2,6),
  (2,7),
  (2,8),
  (2,9),
  (1,10),
  (1,11)
;

INSERT INTO ordenes(comprador_id, status, direccion_id, fecha_compra, fecha_entrega) VALUES
  (2, 'Pagada', 2,  DATE_ADD(NOW(), INTERVAL 5 DAY),  DATE_ADD(NOW(), INTERVAL 5 DAY))
;

INSERT INTO orden_producto(orden_id, producto_id, precio_final, cantidad) VALUES
  (1, 1, 1200.00, 1)
;
