<?php
require_once('../config/cargador.php');

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Producto;

$prod = htmlentities($_GET['product'] ?? '');
$producto = Producto::consultar($prod);

$sesion = new Sesion();
$productosSesion = $sesion->obtener('productos') ?? [];
$usuario = $sesion->obtener('usuario') ?? [];

include Router::direccion('/plantillas/header.php');
?>

<div class="row">
  <div class="col col-sm-6 col-12">
    <img src="<?php Router::rutaImagenWeb($producto->imgProducto != "" ? $producto->imgProducto : "productos/default.png")?>" alt="Imagen Producto" class="img-fluid">
  </div>

  <div class="col col-sm-6 col-12">
    <h3><?php echo $producto->nombre; ?></h3>
    <br>

    <div class="row">
      <div class="col col-6">
        <h4>$<?php echo $producto->precio; ?></h4>
      </div>
    
      <div class="col col-6">
        <?php
          if(!empty($usuario)){
        ?>
        <form action="./carritoCompra/agregar.php" class="agregar form-inline" method="POST">
          <div class="input-group">
            <div class="input-group-append">
              <button class="btn btn-primary" type="submit">Agregar al carrito</button>
            </div>
          </div>
        </form>
      <?php
        } else {
      ?>
        <a class="btn btn-primary" href="login.php">Iniciar Sesion</a>
      <?php
        }
      ?>
      </div>
    </div>
    <br>

    <?php echo $producto->descripcion; ?>
    <br>
    <br>

    <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  </div>

</div>

<?php
include Router::direccion('/plantillas/footer.php');
?>