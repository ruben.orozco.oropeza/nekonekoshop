<?php
include '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Usuario;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
$admin = 0;
if(!empty($usuario)) {
  $admin = $usuario->admin;
  if($admin != 1) {
    Router::redireccionar('index.php');
  }
} else {
  Router::redireccionar('index.php');
}

if (Router::esGet()) {
  Router::redireccionar('usuarios/index.php');
}

//Procesar Editar
if (!empty($_POST['nombre'])) {
  $usuario = $_POST;
  $usuario = new Usuario($usuario);
  $correcto = $usuario->guardar();
  Router::redireccionar('usuarios/index.php');
}
