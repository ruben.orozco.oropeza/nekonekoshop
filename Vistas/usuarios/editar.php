<?php
require_once('../../config/cargador.php');

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Usuario;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
$admin = 0;
if(!empty($usuario)) {
  $admin = $usuario->admin;
  if($admin != 1) {
    Router::redireccionar('index.php');
  }
} else {
  Router::redireccionar('index.php');
}

$usuarioEdit = new Usuario();
if (!empty($_GET['usuario_id'])) {
  $id = $_GET['usuario_id'];
  $usuarioEdit = Usuario::consultar($id);
}

include Router::direccion('plantillas/header.php');
?>
<div class="row">
  <form class="form-horizontal" action="guardar.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="usuario_id" value="<?php echo $usuarioEdit->usuarioId;?>">
    <div class="form-group">
      <label  class="col-md-8 offset-md-2" for="nombre">Nombre:</label>
      <div  class="col-md-8 offset-md-2">
        <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $usuarioEdit->nombre;?>">
      </div>
    </div>
    <br>
    <div class="form-group">
      <label class="col-md-8 offset-md-2" for="nombre_usuario">Nombre Usuario:</label>
      <div class="col-md-8 offset-md-2">
        <input type="text" class="form-control" name="nombre_usuario" value="<?php echo $usuarioEdit->nombreUsuario;?>">
      </div>
    </div>
    <br>
    <div class="form-group">
      <label class="col-md-8 offset-md-2" for="password">Password:</label>
      <div class="col-md-8 offset-md-2">
        <input type="password" class="form-control" name="password" value="<?php echo $usuarioEdit->password;?>">
      </div>
    </div>
    <br>
    <div class="form-group">
      <label class="col-md-8 offset-md-2" for="email">E-Mail:</label>
      <div class="col-md-8 offset-md-2">
        <input type="text" class="form-control" name="email" value="<?php echo $usuarioEdit->email;?>">
      </div>
    </div>
    <br>
    <div class="form-group">
      <label class="col-md-8 offset-md-2" for="estado">Rol:</label>
      <div class="col-md-8 offset-md-2">
        <select name="admin" class="form-control" value="<?php echo $usuarioEdit->admin;?>">
          <option value="0">Cliente</option>
          <option value="1">Administrador</option>
        </select>
      </div>
    </div>
    <br>
    <div class="col-md-3 offset-md-6">
    <button type="submit" class="btn btn-primary pull-right">Guardar</button>
      <a href="index.php" type="submit" class="btn btn-secondary pull-right">Cancelar</a>
    </div>
  </form>
</div>

<?php
include Router::direccion('plantillas/footer.php');
?>