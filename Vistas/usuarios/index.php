<?php
require_once('../../config/cargador.php');

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Usuario;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
$admin = 0;
if(!empty($usuario)) {
  $admin = $usuario->admin;
  if($admin != 1) {
    Router::redireccionar('index.php');
  }
} else {
  Router::redireccionar('index.php');
}

$resultados = Usuario::listar();

include Router::direccion('plantillas/header.php');
?>

<div class="row row-cols-2">
  <h4>Administrar Usuarios</h4>
  <a href="editar.php" class="btn btn-info">Agregar usuario</a>
</div>
<br>

<table class="table table-hover">
  <thead class="table-dark">
    <tr>
      <th>Editar</th>
      <th>Id</th>
      <th>Nombre de Usuario</th>
      <th>Email</th>
      <th>Nombre</th>
      <th>Rol</th>
      <th>Eliminar</th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach($resultados as $renglon) {
    ?>
      <tr>
        <td>
          <form action="editar.php" method="GET" class="form">
            <input type="hidden" name="usuario_id" value="<?php echo $renglon['usuario_id'];?>">
            <input type="submit" value="Editar" class="btn btn-primary">
          </form>
        </td>

        <td><?php echo $renglon['usuario_id']; ?></td>
        <td><?php echo $renglon['nombre_usuario']; ?></td>
        <td><?php echo $renglon['email']; ?></td>
        <td><?php echo $renglon['nombre']; ?></td>
        <td><?php echo $renglon['admin']==1 ? "Admin" : "User"; ?></td>

        <td>
          <form action="eliminar.php" method="POST" class="form">
            <input type="hidden" name="usuario_id" value="<?php echo $renglon['usuario_id']; ?>">
            <input type="submit" value="Eliminar" class="btn btn-danger">
          </form>
        </td>
      </tr>
    <?php
      }
    ?>
  </tbody>
</table>

<?php
include('../plantillas/footer.php');
?>