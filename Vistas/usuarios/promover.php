<?php
include '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Usuario;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
$admin = 0;
if(!empty($usuario)) {
  $admin = $usuario->admin;
  if($admin != 1) {
    Router::redireccionar('index.php');
  }
} else {
  Router::redireccionar('index.php');
}

if (Router::esGet()) {
    Router::redireccionar('usuarios/index.php');
}

$usuarioEdit = new Usuario();
if (!empty($_POST['usuario_id'])) {
  $usuarioEdit = Usuario::consultar($_POST['usuario_id']);
  $usuarioEdit->admin = true;
  var_dump($usuarioEdit);
  $usuarioEdit->guardar(true);
}

Router::redireccionar('usuarios/index.php');