<?php
include '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Usuario;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
$admin = 0;
if(!empty($usuario)) {
  $admin = $usuario->admin;
  if($admin != 1) {
    Router::redireccionar('index.php');
  }
} else {
  Router::redireccionar('index.php');
}

if (Router::esGet()) {
  Router::redireccionar('usuarios/index.php');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $usuarioDelete = new Usuario();
  $usuarioDelete->usuarioId = $_POST['usuario_id'];
  var_dump($usuarioDelete);
  $usuarioDelete->borrar();
}

Router::redireccionar('usuarios/index.php');