<?php
require_once('../../config/cargador.php');

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Producto;
use Modelos\OrdenProducto;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
if(empty($usuario)) {
  Router::redireccionar('index.php');
}

if (Router::esPost()) {
  Router::redireccionar('ordenes/index.php');
}

$productos = OrdenProducto::listar($_GET['orden_id'] ?? '0');
$productosTotal = 0.00;

foreach ($productos as $producto) {
  $productosTotal += (float)$producto['precio_final'];
}

include Router::direccion('plantillas/header.php');
?>

<h4>Productos de la Orden</h4>
<br>
<h4>Compra Total : <b>$<?php echo $productosTotal; ?></b></h4>
<br>

<table class="table table-hover">
  <thead class="table-dark">
    <tr>
      <th>Producto</th>
      <th>Cantidad</th>
      <th>Precio</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach ($productos as $producto) {
        $p = Producto::consultar($producto['producto_id']);
        $nombre = $p->nombre ?? "";
    ?>
      <tr>
        <td><?php echo $nombre; ?></td>
        <td><?php echo $producto['cantidad']; ?></td>
        <td>$<?php echo number_format((float)$producto['precio_final'] / (int)$producto['cantidad'], 2, '.', ''); ?></td>
        <td>$<?php echo number_format((float)$producto['precio_final'], 2, '.', ''); ?></td>
      </tr>
    <?php
      }
    ?>
  </tbody>
</table>
<br>
<a href="index.php" class="btn btn-primary">Regresar</a>

<?php
include Router::direccion('plantillas/footer.php');
?>