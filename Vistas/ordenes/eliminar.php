<?php
include_once '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Orden;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
if(empty($usuario)) {
  Router::redireccionar('index.php');
}

if (Router::esGet()) {
  Router::redireccionar('ordenes/index.php');
}

$orden = new Orden($_POST);
$orden->enviarOrden();
Router::redireccionar('ordenes/index.php');