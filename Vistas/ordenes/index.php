<?php
include_once '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Orden;
use Modelos\OrdenProducto;
use Modelos\Direccion;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
if(empty($usuario)) {
  Router::redireccionar('index.php');
}

$ordenes = Orden::listar($usuario->usuarioId);

include Router::direccion('plantillas/header.php');
?>

<h4>Administrar Ordenes</h4>
<br>

<table class="table table-hover">
  <thead class="table-dark">
    <tr>
      <th>#</th>
      <th>Direccion</th>
      <th>Status</th>
      <th>Total</th>
      <th></th>
      <th></th>
    </tr>
  </thead>

  <tbody>
    <?php
      foreach($ordenes as $orden) {
        $direccion = Direccion::consultar($orden["direccion_id"]);
        $dir = $direccion->nombre ?? "";
        $productos = OrdenProducto::listar($orden["orden_id"] ?? '0');
        $productosTotal = 0.00;

        foreach ($productos as $producto) {
          $productosTotal += (float)$producto['precio_final'];
        }
    ?>
    <tr>
      <td><?php echo $orden["orden_id"]; ?></td>
      <td><?php echo $dir; ?></td>
      <td><?php echo $orden["status"]; ?></td>
      <td>$<?php echo $productosTotal; ?></td>
      <th>
        <form action="verProductos.php" method="GET" class="form">
          <input type="hidden" name="orden_id" value="<?php echo $orden["orden_id"]; ?>">
          <input type="submit" value="Ver Productos" class="btn btn-primary">
        </form>
      </th>
      <td>
        <form action="eliminar.php" method="POST" class="form">
          <input type="hidden" name="orden_id" value="<?php echo $orden["orden_id"]; ?>">
          <input type="submit" value="Enviar" class="btn btn-success">
        </form>
      </td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>

<?php
include Router::direccion('plantillas/footer.php');
?>