<?php
require_once('../config/cargador.php');

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Producto;

$busqueda = htmlentities($_GET['buscar'] ?? '');
$productos = Producto::buscar($busqueda);

$sesion = new Sesion();
$productosSesion = $sesion->obtener('productos') ?? [];

include Router::direccion('/plantillas/header.php');
?>

<div id="productCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="<?php Router::rutaImagenWeb("banner1.png");?>" alt="Slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php Router::rutaImagenWeb("banner2.png");?>" alt="Slide">
    </div>
  </div>
  <a class="carousel-control-prev" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  </a>
  <a class="carousel-control-next" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
  </a>
</div>
<br>

<div class="row row-cols-md-3 row-cols-lg-4 row-cols-xl-6" id="productos">
  <?php
    foreach($productos as $producto) {
      $nuevaCantidad = htmlentities($productosSesion[$producto['producto_id']]['cantidad'] ?? '0');
  ?>
      <div class="col">
        <div class="card">
          <a href="detalles.php?product=<?php echo $producto['producto_id']; ?>">
            <img src="<?php Router::rutaImagenWeb($producto["img_producto"] != "" ? $producto["img_producto"] : "productos/default.png");?>" class="card-img-top" alt="Imagen Producto">
          </a>
          <div class="card-body" id="producto-1">
            <h5 class="card-title">
              <?php echo $producto['nombre']; ?>
              <br>
              <span>$<?php echo $producto['precio']; ?></span>
            </h5>
          </div>
        </div>
      </div>
  <?php
    }
  ?>

</div>

<?php
include Router::direccion('/plantillas/footer.php');
?>

<style>
  .card-img-top {
    width: 100%;
    height: 15vw;
    object-fit: cover;
  }
</style>