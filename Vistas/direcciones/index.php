<?php
include_once '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Direccion;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
if(empty($usuario)) {
  Router::redireccionar('index.php');
}

$direcciones = Direccion::listar($usuario->usuarioId);

include Router::direccion('plantillas/header.php');
?>

<div class="row row-cols-2">
  <h4>Administrar Direcciones</h4>
  <a href="editar.php" class="btn btn-info">Agregar dirección</a>
</div>
<br>

<table class="table table-hover">
  <thead class="table-dark">
    <tr>
      <th>Editar</th>
      <th>Nombre</th>
      <th>Calle Numero</th>
      <th>Cp</th>
      <th>Colonia</th>
      <th>Municipio</th>
      <th>Estado</th>
      <th></th>
    </tr>
  </thead>

  <tbody>
    <?php
      foreach($direcciones as $direccion) {
    ?>
    <tr>
      <td>
        <form action="editar.php" method="GET" class="form">
          <input type="hidden" name="direccion_id" value="<?php echo $direccion["direccion_id"]; ?>">
          <input type="submit" value="Editar" class="btn btn-primary">
        </form>
      </td>
      <td><?php echo $direccion["nombre"]; ?></td>
      <td><?php echo $direccion["calle_numero"]; ?></td>
      <td><?php echo $direccion["cp"]; ?></td>
      <td><?php echo $direccion["colonia"]; ?></td>
      <td><?php echo $direccion["municipio"]; ?></td>
      <td><?php echo $direccion["estado"]; ?></td>
      <td>
        <form action="eliminar.php" method="POST" class="form">
          <input type="hidden" name="direccion_id" value="<?php echo $direccion["direccion_id"]; ?>">
          <input type="submit" value="Eliminar" class="btn btn-danger">
        </form>
      </td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>

<?php
include Router::direccion('plantillas/footer.php');
?>