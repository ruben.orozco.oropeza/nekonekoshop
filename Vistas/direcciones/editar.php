<?php
include_once '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Direccion;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
if(empty($usuario)) {
  Router::redireccionar('index.php');
}

$direccionId = htmlentities($_GET['direccion_id'] ?? '0');
$direccion = Direccion::consultar($direccionId);

include Router::direccion('/plantillas/header.php');
?>

<form class="form" action="guardar.php" method="POST" enctype="multipart/form-data">
  <input type="hidden" name="usuario_id" value="<?php echo $usuario->usuarioId; ?>">
  <input type="hidden" name="direccion_id" value="<?php echo $direccionId; ?>">
  <div class="form-group">
    <label class="col-md-8 offset-md-2" for="nombre">Nombre:</label>
    <div class="col-md-8 offset-md-2">
      <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $direccion->nombre ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-8 offset-md-2" for="ap">Calle y numero:</label>
    <div class="col-md-8 offset-md-2">
      <input type="text" class="form-control" name="calle_numero" value="<?php echo $direccion->calleNumero ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-8 offset-md-2" for="am">CP:</label>
    <div class="col-md-8 offset-md-2">
      <input type="text" class="form-control" name="cp" value="<?php echo $direccion->cp ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-8 offset-md-2" for="correo">Colonia:</label>
    <div class="col-md-8 offset-md-2">
      <input type="text" class="form-control" name="colonia" value="<?php echo $direccion->colonia ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-md-8 offset-md-2" for="pwd">Municipio:</label>
    <div class="col-md-8 offset-md-2">
      <input type="text" class="form-control" name="municipio" value="<?php echo $direccion->municipio ?>">
    </div>
  </div>


  <div class="form-group">
    <label class="col-md-8 offset-md-2" for="pwd">Estado:</label>
    <div class="col-md-8 offset-md-2">
      <input type="text" class="form-control" name="estado" value="<?php echo $direccion->estado ?>">
    </div>
  </div>
  <br>
  <div class="col-md-6 offset-md-6">
    <button type="submit" class="btn btn-primary pull-right">Guardar</button>
  </div>
</form>

<?php
include Router::direccion('/plantillas/footer.php');
?>