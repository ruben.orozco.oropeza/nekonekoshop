<?php
include_once '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Direccion;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
if(empty($usuario)) {
  Router::redireccionar('index.php');
}

if (Router::esGet()) {
  Router::redireccionar('direcciones/index.php');
}

$direccion = new Direccion($_POST);
$direccion->borrar();
Router::redireccionar('direcciones/index.php');