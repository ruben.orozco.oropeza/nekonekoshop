<?php
require_once('../../config/cargador.php');

use Controladores\Router;

include Router::direccion('/plantillas/header.php');
include Router::direccion('/carritoCompra/verCarrito.php');
include Router::direccion('/plantillas/footer.php');