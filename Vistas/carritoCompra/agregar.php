<?php
include '../../config/cargador.php';
use Controladores\Router;
use Controladores\Sesion;

if (Router::esGet()) {
  Router::redireccionar('index.php');
}

$sesion = new Sesion();
$productoId = htmlspecialchars($_POST['producto_id'] ?? '');

if (!empty($productoId)) {
  $sesion->insertarProducto($productoId, $_POST);
}

Router::redireccionar('index.php');
