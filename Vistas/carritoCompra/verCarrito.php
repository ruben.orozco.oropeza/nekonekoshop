<?php
use Controladores\Sesion;

$sesion = new Sesion();
$productosSesion = $sesion->obtener('productos') ?? [];

$productosTotal = 0;
foreach($productosSesion as $producto) {
  $productosTotal += $producto['precio_final'] * $producto['cantidad'];
}

?>

<form action="comprar.php" method="POST">
  <input type="hidden" name="usuario_id" value="1">
  <div class="row row-cols-2">
    <h4>Compra Total : $<?php echo $productosTotal; ?></h4>
    <input type="submit" class="btn btn-info" value="Comprar">
  </div>
</form>
<br>

<table class="table table-hover">
  <thead class="table-dark">
    <tr>
      <th>Producto</th>
      <th>Cantidad</th>
      <th>Precio</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach ($productosSesion as $producto) {
    ?>
    <tr>
      <td><?php echo $producto['producto_id']; ?></td>
      <td><?php echo $producto['cantidad']; ?></td>
      <td><?php echo $producto['precio_final']; ?></td>
      <td><?php echo $producto['precio_final'] * $producto['cantidad']; ?></td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>