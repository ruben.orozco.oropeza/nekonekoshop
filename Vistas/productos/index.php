<?php
include_once '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Producto;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
$admin = 0;
if(!empty($usuario)) {
  $admin = $usuario->admin;
  if($admin != 1) {
    Router::redireccionar('index.php');
  }
} else {
  Router::redireccionar('index.php');
}

$productos = Producto::listar();

include Router::direccion('plantillas/header.php');
?>
<div class="row row-cols-2">
  <h4>Administrar Productos</h4>
  <a href="editar.php" class="btn btn-info">Agregar producto</a>
</div>
<br>

<table class="table table-hover">
  <thead class="table-dark">
    <tr>
      <th>Editar</th>
      <th>Imagen</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Borrar</th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach($productos as $producto) {
    ?>
    <tr>
      <td>
        <form action="editar.php" method="GET" class="form">
          <input type="hidden" name="producto_id" value="<?php echo $producto["producto_id"]; ?>">
          <input type="submit" value="Editar" class="btn btn-primary">
        </form>
      </td>
      <td><img width="60px" src="<?php Router::rutaImagenWeb($producto["img_producto"] != "" ? $producto["img_producto"] : "productos/default.png");?>" alt="Img"></td>
      <td><?php echo $producto["nombre"]; ?></td>
      <td>
        <form action="editarPrecio.php" method="POST" class="form-inline">
          <div class="form-group mx-sm-3 mb-2">
            <input type="hidden" name="producto_id" value="<?php echo $producto["producto_id"]; ?>">
            <input type="number" step="0.01" name="precio" class="form-control" value="<?php echo $producto["precio"]; ?>">
            <button type="submit" class="btn btn-primary mb-2">Actualizar</button>
          </div>
        </form>
      </td>
      <td>
        <form action="eliminar.php" method="POST" class="form">
          <input type="hidden" name="producto_id" value="<?php echo $producto["producto_id"]; ?>">
          <input type="submit" value="Eliminar" class="btn btn-danger">
        </form>
      </td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>

<?php
include Router::direccion('plantillas/footer.php');
?>