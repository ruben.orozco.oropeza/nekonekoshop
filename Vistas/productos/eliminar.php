<?php
include_once '../../config/cargador.php';

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Producto;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
$admin = "0";
if(!empty($usuario)) {
  $admin = $usuario->admin;
  if($admin != "1") {
    Router::redireccionar('index.php');
  }
} else {
  Router::redireccionar('index.php');
}

if (Router::esGet()) {
  Router::redireccionar('productos/index.php');
}

$producto = new Producto($_POST);
$producto->borrar();
Router::redireccionar('productos/index.php');
?>