<?php
require_once('../../config/cargador.php');

use Controladores\Router;
use Controladores\Sesion;
use Modelos\Producto;

$sesion = new Sesion();
$usuario = $sesion->obtener('usuario') ?? [];
$admin = "0";
if(!empty($usuario)) {
  $admin = $usuario->admin;
  if($admin != "1") {
    Router::redireccionar('index.php');
  }
} else {
  Router::redireccionar('index.php');
}

$productoId = htmlentities($_GET['producto_id'] ?? '0');
$producto = Producto::consultar($productoId);

include Router::direccion('plantillas/header.php');
?>

<div class="row">
  <form class="form-horizontal" action="guardar.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="producto_id" value="<?php echo $producto->productoId; ?>">
    <div class="form-group">
      <label class="col-md-8 offset-md-2" for="nombre">Nombre:</label>
      <div class="col-md-8 offset-md-2">
        <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $producto->nombre; ?>">
      </div>
    </div>
    <br>

    <div class="form-group">
      <label class="col-md-8 offset-md-2" for="descripcion">Descripcion:</label>
      <div class="col-md-8 offset-md-2">
        <textarea name="descripcion" class="form-control" rows="5" value="<?php echo $producto->descripcion; ?>"><?php echo $producto->descripcion; ?></textarea>
      </div>
    </div>
    <br>

    <div class="form-group">
      <label class="col-md-8 offset-md-2" for="precio">Precio:</label>
      <div class="col-md-8 offset-md-2">
        <input type="number" min="1" step="0.01" class="form-control" name="precio" value="<?php echo $producto->precio; ?>">
      </div>
    </div>
    <br>

    <div class="form-group">
      <label class="col-md-8 offset-md-2" for="img_producto">Imagen:</label>
      <input type="hidden" name="img_producto" value="<?php echo $producto->imgProducto; ?>">
      <div class="col-md-8 offset-md-2">
        <input type="file" class="form-control" name="img_producto">
      </div>
    </div>
    <br>

    <div class="form-group">
      <div class="col-md-8 offset-md-2 text-center">
        <button type="submit" class="btn btn-primary pull-right">Guardar</button>
        <a href="index.php" type="submit" class="btn btn-secondary pull-right">Cancelar</a>
      </div>
    </div>
  </form>
</div>

<?php
include Router::direccion('plantillas/footer.php');
?>