<?php
define("RUTA_BASE_WEB", '/~ruben/nekonekoshop');
define("RUTA_BASE", '/home/ruben/public_html/nekonekoshop');

spl_autoload_register('cargador');


function cargador($clase) {
    $RUTA = RUTA_BASE . RUTA_BASE_WEB;
    $EXTENSION = ".php";
    $clase = str_replace('\\', DIRECTORY_SEPARATOR, $clase);
    $clase = str_replace('/', DIRECTORY_SEPARATOR, $clase);

    $rutaArchivo = "{$RUTA}{$clase}{$EXTENSION}";
    if (file_exists($rutaArchivo)) {
        require_once($rutaArchivo);
        return;
    }

    $rutaArchivo = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "{$clase}{$EXTENSION}";
    if (file_exists($rutaArchivo)) {
        require_once($rutaArchivo);
        return;
    }

    $rutaArchivo = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "{$clase}{$EXTENSION}";
    if (file_exists($rutaArchivo)) {
        require_once("{$RUTA}{$clase}{$EXTENSION}");
        return;
    }

    echo $clase;
}